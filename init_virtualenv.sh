#!/bin/bash
set -e

VENV=${2:-.env}

cd "$(dirname "$0")"

echo "[$(date)] Removing existing virtualenv if it exists."
[ -d $VENV ] && rm -Rf $VENV

echo "[$(date)] Creating virtual environment."
python3.7 -m venv $VENV

echo "[$(date)] Activating virtual environment."
. $VENV/bin/activate

echo "[$(date)] Upgrading pip."
pip install -U pip setuptools wheel

pip install Cython

echo "[$(date)] Installing pip requirements."
pip install -r pip-requirements.txt -r pip-requirements-test.txt
