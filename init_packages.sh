#!/bin/bash
apt-get -yq update
apt-get -yq install software-properties-common
add-apt-repository -y ppa:deadsnakes/ppa
apt-get -yq update
apt-get install -yq `cat "packages.txt" | sed '/^#/ d' | tr "\\n" " "`
