import os
import sys
import logging
import copy
import threading
import queue
import tempfile
from subprocess import getstatusoutput

import aubio
import numpy as np
import pyaudio

import music21
# from music21.converter.subConverters import ConverterMusicXML
from music21.lily.lilyObjects import LyOutputDefBody as _LyOutputDefBody, LyLilypondHeaderBody as _LyLilypondHeaderBody, LyAssignment, LyIdentifierInit
from music21.lily.translate import LilypondConverter as _LilypondConverter
from music21.stream import Measure
from music21 import layout
from music21.lily import lilyObjects as lyo

from svgclip import clip

logging.basicConfig()
logging.root.setLevel(logging.NOTSET)
logging.basicConfig(level=logging.NOTSET)
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

music21.environment.set("musicxmlPath", "/usr/bin/musescore")
music21.environment.set("midiPath", "/usr/bin/timidity")
music21.environment.set("musescoreDirectPNGPath", "/usr/bin/musescore")
music21.environment.set("lilypondPath", '/usr/bin/lilypond')
# music21.environment.set("musicxmlPath", "/bin/true") # headless

DATA_DIR = os.path.expanduser('~/.notoriety')

SILENCE = 'silence'


def rgb_to_int(s):
    """
    Converts an RGB color string of the format "#rrggbb" to the equivalent Python tuple where each color is a float bounded in the range [0:1].
    """
    if not isinstance(s, str) or not s.startswith('#') or not len(s) == 7:
        raise NotImplementedError
    r = int(s[1:3], 16)
    g = int(s[3:5], 16)
    b = int(s[5:7], 16)
    return r/255., g/255., b/255.


# Monkey-patch Music21's Lilypond converter to remove indent.
class LyOutputDefBody(_LyOutputDefBody):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if not self.outputDefIdentifier and self.outputDefHead and self.outputDefHead.defType == 'paper': # pylint: disable=access-member-before-definition
            self.outputDefIdentifier = '\n  indent = 0\\cm\n'

music21.lily.lilyObjects.LyOutputDefBody = LyOutputDefBody


class LyLilypondHeaderBody(_LyLilypondHeaderBody):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # self.assignments.append('tagline = ""') # Disable "Music engraved by" footer signature auto-added by Lilypond.
        self.assignments.append(LyAssignment(assignmentId='tagline', identifierInit=LyIdentifierInit(string='')))

music21.lily.lilyObjects.LyLilypondHeaderBody = LyLilypondHeaderBody

# Monkey-patch converter to output correct note color syntax.
#TODO: Remove this when/if Music21 is fixed.
class LilypondConverter(_LilypondConverter):

    # Overrode entire method because bug is in hard-coded section that otherwise can't be accessed.
    def lySimpleMusicFromNoteOrRest(self, noteOrRest):
        r'''
        returns a lilyObjects.LySimpleMusic object for the generalNote containing...

            LyEventChord   containing
            LySimpleChordElements containing
            LySimpleElement containing
            LyPitch  AND
            LyMultipliedDuration containing:

                LyMultipliedDuration containing
                LyStenoDuration

        does not check for tuplets.  That's in
        appendContextFromNoteOrRest

        read-only property that returns a string of the lilypond representation of
        a note (or via subclassing, rest or chord)

        >>> conv = lily.translate.LilypondConverter()

        >>> n0 = note.Note("D#5")
        >>> n0.pitch.accidental.displayType = 'always'
        >>> n0.pitch.accidental.displayStyle = 'parentheses'
        >>> n0.style.color = 'blue'
        >>> sm = conv.lySimpleMusicFromNoteOrRest(n0)
        >>> print(sm)
        \color "blue" dis'' ! ? 4

        Now make the note disappear...

        >>> n0.style.hideObjectOnPrint = True
        >>> sm = conv.lySimpleMusicFromNoteOrRest(n0)
        >>> print(sm)
        s 4
        '''
        c = noteOrRest.classes

        simpleElementParts = []
        if noteOrRest.hasStyleInformation:
            if noteOrRest.style.color and noteOrRest.style.hideObjectOnPrint is False:
                # colorLily = r'\color "' + noteOrRest.style.color + '" '
                # colorLily = r'\override NoteHead.color = #(rgb-color 0 1 0)' + '\n' + r'\override Stem.color = #(rgb-color 0 1 0)' + '\n'
                try:
                    rgb_list = ' '.join(map(str, rgb_to_int(noteOrRest.style.color)))
                    colorLily = f'\\override NoteHead.color = #(rgb-color {rgb_list})\n\\override Stem.color = #(rgb-color {rgb_list})\n'
                except NotImplementedError:
                    colorLily = r'\color "' + noteOrRest.style.color + '" '
                simpleElementParts.append(colorLily)

        if 'Note' in c:
            if not noteOrRest.hasStyleInformation or noteOrRest.style.hideObjectOnPrint is False:
                lpPitch = self.lyPitchFromPitch(noteOrRest.pitch)
                simpleElementParts.append(lpPitch)
                if noteOrRest.pitch.accidental is not None:
                    if noteOrRest.pitch.accidental.displayType == 'always':
                        simpleElementParts.append('! ')
                    if noteOrRest.pitch.accidental.displayStyle == 'parentheses':
                        simpleElementParts.append('? ')
            else:
                simpleElementParts.append("s ")

        elif "SpacerRest" in c:
            simpleElementParts.append("s ")
        elif 'Rest' in c:
            if noteOrRest.hasStyleInformation and noteOrRest.hideObjectOnPrint:
                simpleElementParts.append("s ")
            else:
                simpleElementParts.append("r ")

        lpMultipliedDuration = self.lyMultipliedDurationFromDuration(noteOrRest.duration)
        simpleElementParts.append(lpMultipliedDuration)

        if 'NotRest' in c and noteOrRest.beams is not None and noteOrRest.beams:
            if noteOrRest.beams.beamsList[0].type == 'start':
                simpleElementParts.append("[ ")
            elif noteOrRest.beams.beamsList[0].type == 'stop':
                simpleElementParts.append("] ")  # no start-stop in music21...

        simpleElement = lyo.LySimpleElement(parts=simpleElementParts)

        postEvents = self.postEventsFromObject(noteOrRest)

        evc = lyo.LyEventChord(simpleElement, postEvents=postEvents)
        mlSM = lyo.LySimpleMusic(eventChord=evc)

        return mlSM

music21.lily.translate.LilypondConverter = LilypondConverter


def crop_svg(*args):
    """
    Modifies all specified SVG files in-place by cropping them to their minimum bounding box.
    """
    for fn in args:
        clip(fn, fn, margin=10)


def load_score_line_breaks(score, abc_path=None):
    """
    Modifies a score in-place by adding line breaks corresponding to the ABC's original formatting.
    By default Music21 ignores the implicit layout in ABC, causing the original staff layout to be lost.
    This re-parses the score's ABC file, deduces the line breaks and inserts them into the measure layout.
    """
    abc_path = abc_path or score.filePath
    assert str(abc_path).endswith('.abc'), 'Only scores sourced from ABC files are supported.'

    measures_with_breaks = set()
    with open(abc_path) as fin:
        measure_count = 0
        for line in fin.readlines():
            line = line.strip()
            # Ignore blanks.
            if not line:
                continue
            # Ignore comments.
            if line.startswith('%'):
                continue
            # Ignore headers.
            if len(line) >= 2 and line[1] == ':':
                continue
            measure_count += line.count('|')
            measures_with_breaks.add(measure_count)

    for el in score.recurse():
        if isinstance(el, Measure) and el.measureNumber in measures_with_breaks:
            logger.debug('Inserting break for %s.', el.measureNumber)
            el.append(layout.SystemLayout(isNew=True))

class Latch:
    """
    Tracks a value stream and returns a value if it's shown to appear in enough consecutive instances.
    """

    def __init__(self, size=3):
        self.q = []
        self.size = size
        self._last_value = None

    def add(self, value):
        self.q.append(value)
        if len(self.q) >= self.size:
            self.q.pop(0)

    @property
    def value(self):
        if len(self.q) >= self.size - 1 and len(set(self.q)) == 1 and self.q[0]:
            self._last_value = self.q[0]
        return self._last_value


class Notoriety:
    """
    Wrapper around an ABC file.
    """

    QueueEmpty = queue.Empty
    QueueFull = queue.Full

    def __init__(self, path=None):
        assert not path or os.path.isfile(path)
        self.path = path
        self.score = None
        if self.path:
            self.score = music21.converter.parse(self.path)
        self.q = queue.Queue(maxsize=50)
        self._listening = False
        self._listener_thread = None

    def find_tempo(self):
        """
        Returns the tempo in the given ABC file, found from the first tempo "Q:" notation.
        """
        #TODO:support multiple Q: marks, indicating changing tempo?
        with open(self.score.filePath) as fin:
            for line in fin.readlines():
                line = line.strip()
                if not line:
                    continue
                if line.startswith('Q:'):
                    tempo = int(line.split(':')[1])
                    return tempo

    @property
    def base_dir(self):
        base_dir, abc_fn = os.path.split(self.path)
        return base_dir

    @property
    def score_name(self):
        base_dir, abc_fn = os.path.split(self.path)
        score_name, _ = os.path.splitext(abc_fn)
        return score_name

    def play(self):
        """
        Produces MIDI audio.
        """
        logger.info('Playing %s.', self.score_name)
        self.score.show('midi')
        logger.info('Done playing.')

    def start_listener(self):
        if self._listener_thread:
            logger.info('Listener already running.')
            return
        logger.info('Starting listener.')
        self._listener_thread = threading.Thread(target=self.listen, kwargs=dict(use_queue=True))
        self._listener_thread.setDaemon(True)
        self._listener_thread.start()
        logger.info('Listener started.')

    def stop_listener(self):
        if not self._listener_thread:
            logger.info('Listener already stopped.')
            return
        logger.info('Waiting for listener to stop.')
        self._listening = False
        self._listener_thread.join()
        self._listener_thread = None
        logger.info('Listener stopped.')
        while not self.q.empty():
            self.q.get_nowait()

    def listen(self, use_queue=False, show=False, volume_thresh=0.05, min_frames=5, show_volume=False):
        """
        Monitors the microphone and displays the closest muscial note.
        """

        p = pyaudio.PyAudio()

        # Open stream.
        stream = p.open(format=pyaudio.paFloat32,
                        channels=1, rate=44100, input=True,
                        # input_device_index=args.input,
                        frames_per_buffer=4096)

        # Aubio's pitch detection.
        pDetection = aubio.pitch("default", 2048, 2048//2, 44100)
        # Set unit.
        pDetection.set_unit("Hz")
        pDetection.set_silence(-40)

        self._listening = True
        found_frames = 0
        pitch_latch = Latch(size=2)
        last_value = None
        while self._listening:

            data = stream.read(1024, exception_on_overflow=False)
            samples = np.fromstring(data, dtype=aubio.float_type)

            # Compute the energy (volume) of the current frame.
            volume = np.sum(samples**2)/len(samples) * 100
            if show_volume:
                print('Volume:', volume)

            # Track historical energy detect when there's meaningful sound.
            if volume > volume_thresh:
                found_frames += 1
                # print('sound with frames:',  found_frames)
            else:
                found_frames -= 1
            found_frames = min(max(found_frames, 0), min_frames)

            # Track consistency of sound or silence.
            if found_frames < min_frames:
                # Silence.
                pitch_latch.add(SILENCE)
            else:
                # Sound.
                frequency = pDetection(samples)[0]
                if frequency:
                    _pitch = music21.pitch.Pitch()
                    _pitch.frequency = frequency
                    pitch_latch.add((_pitch.step, _pitch.octave))
                else:
                    pitch_latch.add(SILENCE)

            consistent_value = pitch_latch.value

            if show:
                if consistent_value != last_value:
                    if isinstance(consistent_value, tuple):
                        consistent_value_str = '%s%s' % consistent_value
                    else:
                        consistent_value_str = consistent_value
                    print('Pitch:', consistent_value_str)

            if use_queue and consistent_value != last_value:
                try:
                    self.q.put_nowait(consistent_value)
                except self.QueueFull:
                    pass

            last_value = consistent_value

    def render(self, temp_dir=None, score=None, index=None, force=False, total=None, svg=False, show=False):
        """
        Converts the music to sheet music in PNG and SVG format.
        """
        score = score or self.score
        score_name = self.score_name

        load_score_line_breaks(score)

        if show:
            score.show()
            return

        temp_dir = temp_dir or self.base_dir

        xml_fn = '%s.xml' % score_name
        xml_fqfn = os.path.join(temp_dir, xml_fn)
        if index is None:
            png_fn = '%s.png' % score_name
        else:
            png_fn = '%s-%s.png' % (score_name, index)
        png_fqfn = os.path.join(temp_dir, png_fn)
        svg_fqfn = os.path.join(temp_dir, os.path.splitext(png_fn)[0]+'.svg')
        ly_fqfn = os.path.join(temp_dir, os.path.splitext(png_fn)[0]+'.ly')
        if not os.path.isfile(png_fqfn) or force:
            logger.info('Rendering for index %s of %s.', index, total)
            if svg:
                tmp_svg_fn = os.path.join(temp_dir, score_name) + '001.svg'
                final_svg_fn = os.path.join(temp_dir, score_name) + '.svg'
                cmd = f'abcm2ps "{self.path}" -g -O "{final_svg_fn}"'
                logger.info(cmd)
                os.system(cmd)
                # TODO:Fix? Tool adds a "001" to the end of the basename? No option to disable?
                os.rename(tmp_svg_fn, final_svg_fn)
                logger.info('Done rendering SVG %s.', final_svg_fn)
            else:
                # TODO:Fix? Music21's ABC-to-MusicXML parser is so comically bad, we can't use it.
                # conv = ConverterMusicXML()
                # png_tmp_fqfn = conv.write(score, 'musicxml', fp=xml_fqfn, subformats=['png'])
                # os.rename(png_tmp_fqfn, png_fqfn)
                # if os.path.isfile(xml_fqfn):
                    # os.remove(xml_fqfn)

                # Export score as MusicXML and reload to work around buggy Music21 ABC parser.
                # Only necessary when using the Musescore backend.
                # tmp_fn = self.get_musicxml_fn()
                # score = music21.converter.parse(tmp_fn)
                # score.insert(0, music21.metadata.Metadata())
                # score.metadata.title = self.score.metadata.title
                # load_score_line_breaks(score, self.score.filePath)

                # Reference. Generates PNG, and notes are correct, but line breaks are wrong and title is wrong.
                # conv = music21.converter.subConverters.ConverterMusicXML()
                # png_tmp_fqfn = conv.write(score, 'musicxml', fp=xml_fqfn, subformats=['png'])
                # os.rename(png_tmp_fqfn, png_fqfn)
                # if os.path.isfile(xml_fqfn):
                    # os.remove(xml_fqfn)

                # for i, note in enumerate(score.recurse().notes):#TODO:remove
                    # note.style.color = '#00FF00'

                conv = music21.converter.subConverters.ConverterLilypond()
                conv.write(score, fmt='lilypond', fp=ly_fqfn, subformats=['png'])
                # TODO:fix? By default, Lilypond outputs xml to the png path, but outputs the correct png to path+'.png', as well as eps files.
                os.rename(ly_fqfn+'.png', png_fqfn)
                os.system(f'rm -f {ly_fqfn}-*.eps')
                os.system(f'rm -f {ly_fqfn}-systems*')
                conv.write(score, fmt='lilypond', fp=ly_fqfn, subformats=['svg'])
                os.rename(ly_fqfn+'.svg', svg_fqfn)
                crop_svg(svg_fqfn)

                logger.info('Done rendering PNG %s.', png_fqfn)
        else:
            logger.info('No need to render %s.', png_fqfn)

    def get_musicxml_fn(self):
        """
        Temporarily converts and outputs the ABC to MusicXML.
        Note we don't use Music21 to do the conversion because fatal bugs prevent it from propoerly parsing ABC.
        """
        temp = tempfile.NamedTemporaryFile(delete=False)
        temp.close()
        temp_xml = f'{temp.name}.xml'
        os.rename(temp.name, temp_xml)
        cmd = f'{sys.executable} abc2xml.py {self.score.filePath} > {temp_xml}'
        logger.info(cmd)
        status, output = getstatusoutput(cmd)
        assert not status, 'Unable to convert file: %s' % output
        return temp_xml

    @property
    def highlight_dir(self):
        return os.path.join(DATA_DIR, 'expansions', self.score_name)

    def iter_notes(self):
        for note in self.score.recurse().notes:
            yield note

    def render_note_highlights(self):
        temp_dir = self.highlight_dir
        os.makedirs(temp_dir, exist_ok=True)
        logger.info('Using highlight directory: %s', temp_dir)
        future_color = '#A7A7A7'
        playing_color = '#00FF00'
        past_color = '#000000'
        score = copy.deepcopy(self.score)

        # Set all notes to future color.
        for i, note in enumerate(score.recurse().notes):
            note.style.color = future_color

        total_images = len(list(score.recurse().notes))*2 + 1
        digit_chars = len(str(total_images))

        # First image, all notes future.
        i = 0
        self.render(score=score, index=('%0'+str(digit_chars)+'i') % (i), temp_dir=temp_dir, total=total_images)

        for i, note in enumerate(score.recurse().notes):

            # Render playing note.
            note.style.color = playing_color
            self.render(score=score, index=('%0'+str(digit_chars)+'i') % (i*2+1), temp_dir=temp_dir, total=total_images)

            # Render played note.
            note.style.color = past_color
            self.render(score=score, index=('%0'+str(digit_chars)+'i') % (i*2+2), temp_dir=temp_dir, total=total_images)

        logger.info('Done rendering highlights.')

    def iter_highlight_images(self):
        temp_dir = self.highlight_dir
        for fn in sorted(os.listdir(temp_dir)):
            if fn.endswith('.png'):
                svg_fn = os.path.join(temp_dir, os.path.splitext(fn)[0] + '.svg')
                if os.path.isfile(svg_fn):
                    yield svg_fn
                else:
                    yield os.path.join(temp_dir, fn)
