#!/usr/bin/env python
"""
Keys:
    p = play/pause
    r = reset
    n = next playlist item
    b = back playlist item
    q/esc = exit
"""
import os
import sys
import logging
import time
import argparse
from collections import defaultdict
from datetime import datetime, date

from dateutil.parser import parse

from svg import Parser, Rasterizer # pylint: disable=no-name-in-module

from server import app

import cairo
import gi
gi.require_version('Rsvg', '2.0') # pylint: disable=wrong-import-position
from gi.repository import Rsvg as rsvg # pylint: disable=no-name-in-module

os.environ['PYGAME_HIDE_SUPPORT_PROMPT'] = "hide"
import pygame # pylint: disable=wrong-import-position

from notoriety import Notoriety, SILENCE # pylint: disable=wrong-import-position

GLOBAL_DATE = (date.max.year, date.max.year)

GREEN = (0, 255, 0)
RED = (255, 0, 0)
BLACK = (0, 0, 0)
YELLOW = (255, 255, 0)
WHITE = (255, 255, 255)
CIRCLE_DIAMETER = 50

BUFFER_H = 25
BUFFER_W = 0

LOG_FN = os.path.expanduser('~/.notoriety/scores.log')

logger = logging.getLogger('tutor_abc')


def text_objects(text, font):
    textSurface = font.render(text, True, BLACK)
    return textSurface, textSurface.get_rect()

def get_base_svg_size(filename):
    """
    Returns the SVG's file's default dimensions.
    """
    svg = Parser.parse_file(filename)
    return svg.width, svg.height

def load_svg(filename, size):
    """
    Loads the given SVG file as a rasterized image at the given size.
    """
    w, h = size
    svg = Parser.parse_file(filename)
    rast = Rasterizer()
    print('rasterizing:', filename, w, h)
    buff = rast.rasterize(svg, w, h)
    image = pygame.image.frombuffer(buff, (w, h), 'RGBA')
    return image


class ChangeTracker:
    """
    Tracks timing when a note changes.
    """

    def __init__(self):
        self.reset()

    def changed(self, next_value):
        """
        Returns true if the given next value doesn't match the previous value.
        """
        return self._value != next_value

    @property
    def value(self):
        return self._value

    @property
    def td(self):
        """
        Returns the difference between the current wall clock time and the time when the value was last changed.
        """
        if self._t0 is None:
            return
        return time.time() - self._t0

    def update(self, value):
        """
        Updates the value being tracked. If a change was made, refreshing timing data.
        """
        if value != self._value:
            self._t0 = time.time()
        self._value = value

    def reset(self):
        """
        Re-initializes internal values.
        """
        self._value = None
        self._t0 = None


class Playlist:
    """
    Manipulates an ABC playlist file.
    """

    def __init__(self, fn=None, ordering=None):
        self.fn = fn
        self._abc_files = []
        if fn:
            base_dir = os.path.split(os.path.abspath(fn))[0]
            with open(fn) as fin:
                for line in fin.readlines():
                    line = line.strip()
                    # Ignore blanks.
                    if not line:
                        continue
                    # Ignore comments.
                    if line.startswith('#'):
                        continue
                    if line.startswith('/'):
                        fqfn = line
                    else:
                        fqfn = os.path.join(base_dir, line)
                    assert os.path.isfile(fqfn), 'File %s in playlist %s does not exist.' % (fqfn, fn)
                    self._abc_files.append(os.path.abspath(fqfn))
        if ordering:
            self._abc_files.sort(key=lambda p: ordering.get(p, 1e9999999), reverse=True)

    def add(self, path):
        self._abc_files.append(path)

    def first(self):
        if self._abc_files:
            return self._abc_files[0]

    def __iter__(self):
        for fn in self._abc_files:
            yield fn

    def __len__(self):
        return len(self._abc_files)

    def __getitem__(self, i):
        return self._abc_files[i]

    def __bool__(self):
        return bool(self._abc_files)


class ABC:
    """
    ABC file manipulator.
    """

    def __init__(self, fn):
        self.fn = fn
        self.headers = []
        self.notes = [] # [[note, note, ...]]
        with open(self.fn) as fin:
            for line in fin.readlines():
                line = line.strip()
                if not line:
                    continue
                if line.startswith('%'):
                    pass

    def create_from_note(self, i):
        pass

class Game:

    WAITING = 'waiting'
    PLAYING = 'playing'
    DONE = 'done'

    def __init__(self, path, ordering=None, scroll=False):
        pygame.init()
        icon = pygame.image.load('icons/icon7.png')
        # icon = pygame.image.load('icons/icon6-300.png')
        # icon = load_svg('icons/icon5.svg', (32, 32))
        pygame.display.set_icon(icon)
        info = pygame.display.Info()
        # self.display_width = None #1800
        # self.display_height = None #768
        self._display_width = info.current_w
        self._display_height = info.current_h
        self.scroll = scroll
        self.playlist_position = 0
        self.playlist = None
        if path.endswith('.pls'):
            self.playlist = Playlist(path, ordering=ordering)
            assert self.playlist, 'Playlist is empty!'
            self.notor = Notoriety(self.playlist.first())
        else:
            self.playlist = Playlist()
            self.playlist.add(path)
            self.notor = Notoriety(path)
        self.playing = False
        self.crashed = False
        self.bpm = None
        self.beat_duration_seconds = None
        self.i = 0
        self.t0 = None
        self.t1 = None
        self.mode = self.WAITING
        self.note_tracker = ChangeTracker()
        self.total_seconds_expected = 0
        self._rendered = {}
        self.reset()

    @property
    def display_width(self):
        return self._display_width

    @property
    def display_height(self):
        return self._display_height - 100 # buffer for system header and footer

    @property
    def td(self):
        if self.t0 and self.t1:
            return self.t1 - self.t0

    def reset(self):
        self.i = 0
        self.playing = False
        self.t0 = None
        self.t1 = None
        self.recorded_score = False
        self.mode = self.WAITING
        self.note_tracker.reset()
        self.notor.stop_listener()
        self._highlight_image_cache = {}
        self.highlight_image_filenames = []
        if not self.scroll:
            if not self._rendered.get(self.playlist_position):
                self.notor.render_note_highlights()
                self._rendered[self.playlist_position] = True
            self.highlight_image_filenames = list(self.notor.iter_highlight_images())
        self.notes = list(self.notor.iter_notes())

        self.bpm = self.notor.find_tempo()
        if self.bpm is None:
            self.bpm = 60
            logger.info('No explicit BPM found. Assuming default of %s.', self.bpm)
        else:
            logger.info('BPM: %s', self.bpm)
        self.beat_duration_seconds = 60/self.bpm # length of a quarter-note

        self.total_seconds_expected = 0
        for note in self.notor.iter_notes():
            self.total_seconds_expected += note.duration.quarterLength * self.beat_duration_seconds
        logger.info('Total seconds expected: %s', self.total_seconds_expected)

        # Sanity check that images match the notes.
        expected_image_count = len(self.notes)*2 + 1
        if not self.scroll:
            actual_image_count = len(self.highlight_image_filenames)
            assert actual_image_count == expected_image_count, 'Expected %i images but %i found.' % (expected_image_count, actual_image_count)

    def get_note_duration(self, note):
        """
        Returns total expected note duration in seconds.
        """
        return note.duration.quarterLength * self.beat_duration_seconds

    def forward(self, force=False):
        """
        Moves the game forward one note.
        """
        if self.playing or force:
            self.i += 1
            self.i = min(self.i, len(self.highlight_image_filenames)-1)
            if self.i == len(self.highlight_image_filenames) - 1:
                self.playing = False
                self.t1 = time.time()
                print('Completion seconds: %s' % self.td)

    def backward(self, force=False):
        """
        Moves the game backward one note.
        """
        if self.playing or force:
            self.i -= 1
            self.i = max(self.i, 0)

    def start_playing(self):
        if self.playing:
            return
        self.playing = True
        self.notor.start_listener()

    def stop_playing(self):
        if not self.playing:
            return
        self.playing = False
        self.notor.stop_listener()

    def get_scaled_image_size(self, width, height):
        if width > height:
            new_w, new_h = self.display_width, int(self.display_width/width*height)
            if new_h > self.display_height:
                new_w, new_h = int(self.display_height/height*width), self.display_height
        else:
            new_w, new_h = int(self.display_height/height*width), self.display_height
            if new_w > self.display_width:
                new_w, new_h = self.display_width, int(self.display_width/width*height)
        return new_w, new_h

    def get_highlight_image(self, i):
        if i not in self._highlight_image_cache:
            fn = self.highlight_image_filenames[i]
            logger.info('Loading image %s.', fn)
            if fn.endswith('.svg'):
                # Load SVG and scale to our display.
                img_width, img_height = get_base_svg_size(fn)
                new_w, new_h = self.get_scaled_image_size(img_width, img_height)
                surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, new_w, new_h)
                ctx = cairo.Context(surface)
                scale_factor = 1.1
                ctx.scale(new_w/img_width*scale_factor, new_h/img_height*scale_factor)
                svg = rsvg.Handle.new_from_file(fn)
                svg.render_cairo(ctx)
                buf = surface.get_data()
                img = pygame.image.frombuffer(buf, (new_w, new_h), "RGBA")
            else:
                # Load static PNG.
                assert fn.endswith('.png'), 'Invalid image format: %s' % fn
                img = pygame.image.load(fn)
                img_width, img_height = img.get_rect().size
                new_w, new_h = self.get_scaled_image_size(img_width, img_height)
                img = pygame.transform.scale(img, (new_w, new_h))
            self._highlight_image_cache[i] = img
        return self._highlight_image_cache[i]

    @property
    def path(self):
        return self.playlist[self.playlist_position]

    def at_playlist_max(self):
        return self.playlist_position == len(self.playlist)-1

    def at_playlist_min(self):
        return self.playlist_position == 0

    def increment_playlist(self):
        if self.at_playlist_max():
            return
        self.reset()
        self.playlist_position += 1
        self.playlist_position = min(self.playlist_position, len(self.playlist)-1)
        logger.info('Incrementing playlist to %i %s.', self.playlist_position, self.path)
        self.notor = Notoriety(self.path)
        self.reset()

    def decrement_playlist(self):
        if self.at_playlist_min():
            return
        self.reset()
        self.playlist_position -= 1
        self.playlist_position = max(self.playlist_position, 0)
        logger.info('Decrementing playlist to %i %s.', self.playlist_position, self.path)
        self.notor = Notoriety(self.path)
        self.reset()

    def record_performance_log(self, path, score):
        if self.recorded_score:
            return
        self.recorded_score = True
        with open(LOG_FN, 'a') as fout:
            now = datetime.now()
            fout.write(f'{now},{path},{score}\n')

    def run(self):

        gameDisplay = pygame.display.set_mode((self._display_width, self._display_height), pygame.NOFRAME)
        # gameDisplay = pygame.display.set_mode((0, 0), pygame.FULLSCREEN|pygame.RESIZABLE)
        pygame.display.set_caption(self.notor.score_name)

        clock = pygame.time.Clock()

        # Load note transition images.

        # highlight_images = []
        # for fn in highlight_image_filenames:
            # logger.info('Loading image %s.', fn)
            # img = pygame.image.load(fn)
            # img_width, img_height = img.get_rect().size
            # if img_width > img_height:
                # img = pygame.transform.scale(img, (display_width, int(display_width/img_width*img_height)))
            # else:
                # img = pygame.transform.scale(img, (int(display_height/img_height*img_width), display_height))
            # highlight_images.append(img)
        # self.highlight_image_filenames = highlight_images

        playlist_change = None
        next_current_pitch = None
        first_wait = True
        pitch_text = ''
        current_pitch = None
        pitch_text_t0 = None
        is_loading = False
        showing_help = True
        while not self.crashed:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    logger.info('Quitting from window.')
                    self.crashed = True
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_q or event.key == pygame.K_ESCAPE:
                        logger.info('Quitting from keypress.')
                        self.crashed = True
                    elif event.key == pygame.K_p:
                        if not self.td:
                            if self.playing:
                                self.stop_playing()
                            else:
                                self.start_playing()
                        showing_help = False
                    elif event.key == pygame.K_r:
                        if self.playing:
                            self.stop_playing()
                            self.reset()
                            self.start_playing()
                        else:
                            self.reset()
                        showing_help = False
                    elif event.key == pygame.K_n:
                        if not self.at_playlist_max():
                            is_loading = True
                            playlist_change = self.increment_playlist
                            showing_help = False
                    elif event.key == pygame.K_b:
                        if not self.at_playlist_min():
                            is_loading = True
                            playlist_change = self.decrement_playlist
                            showing_help = False
                    elif event.key == pygame.K_LEFT:
                        self.backward(force=True)
                        showing_help = False
                    elif event.key == pygame.K_RIGHT:
                        self.forward(force=True)
                        showing_help = False

            check_implicit = True
            note_duration = None
            note_played = None
            if self.scroll:
                pass
            else:
                try:

                    # Find the current pitch event. There may be nothing if nothing's changed.
                    try:
                        current_pitch = self.notor.q.get_nowait() # Can be "silence" or (step, octave).
                    except self.notor.QueueEmpty:
                        if next_current_pitch:
                            current_pitch = next_current_pitch
                            next_current_pitch = None
                        else:
                            raise
                    print('current_pitch:', current_pitch)

                    expected_node_index = int(round(self.i/2))
                    expected_note = self.notes[expected_node_index]
                    expected_duration = expected_note.duration.quarterLength * self.beat_duration_seconds
                    expected = (expected_note.pitch.step, expected_note.pitch.octave)
                    actual = current_pitch

                    # Update state machine when an explicit note event is found.
                    if self.mode == self.WAITING:
                        if self.t0 is None and current_pitch and current_pitch != SILENCE:
                            self.t0 = time.time()
                        if first_wait:
                            logger.info('Position index: %s', self.i)
                            logger.info('Expected note index: %s', expected_node_index)
                            logger.info('Waiting for %s for %s.', expected_note, expected_duration)
                        if actual == expected:
                            logger.info('Correct note detected!')
                            self.note_tracker.update(actual)
                            self.mode = self.PLAYING
                            self.forward()
                            check_implicit = False
                        first_wait = False

                    elif self.mode == self.PLAYING:
                        if actual == SILENCE:
                            logger.info('Note transitioned due to silence.')
                            self.mode = self.WAITING
                            first_wait = True
                            note_duration = self.note_tracker.td
                            note_played = self.note_tracker.value
                            self.forward()
                            check_implicit = False
                        elif actual != self.note_tracker.value:
                            logger.info('Note transitioned due immediate note switch. We were playing %s but then saw %s.', self.note_tracker.value, actual)
                            note_duration = self.note_tracker.td
                            note_played = self.note_tracker.value
                            self.mode = self.WAITING
                            first_wait = True
                            self.note_tracker.update(actual)
                            self.forward()
                            check_implicit = False
                        elif actual == self.note_tracker.value:
                            if self.note_tracker.td >= expected_duration:
                                logger.info('Note transitioned due to completion.')
                                note_duration = self.note_tracker.td
                                note_played = self.note_tracker.value
                                self.mode = self.WAITING
                                first_wait = True
                                self.note_tracker.reset()
                                self.note_tracker.update(actual)
                                self.forward()
                                check_implicit = False
                            else:
                                logger.info('Note has lasted for %s but we need %s.', self.note_tracker.td, expected_duration)
                        else:
                            logger.info('Received %s but currently tracking %s.', actual, self.note_tracker.value)

                except IndexError:
                    self.forward()
                    self.mode = self.DONE
                except self.notor.QueueEmpty:
                    current_pitch = None

                # Update state machine when an implicit note event is found.
                if check_implicit:
                    if self.mode == self.PLAYING and self.note_tracker.td >= expected_duration*1.25:
                        logger.info('Note transitioned due to implicit completion.')
                        note_duration = self.note_tracker.td
                        note_played = self.note_tracker.value
                        self.mode = self.WAITING
                        first_wait = True
                        self.note_tracker.reset()
                        self.note_tracker.update(actual)
                        self.forward()
                        next_current_pitch = note_played

            if note_played:
                note_str = ''.join(map(str, note_played))
                logger.info('Note %s expected to play for %s seconds.', note_str, expected_duration)
                logger.info('Note %s actually played for %s seconds.', note_str, note_duration)

            # Manage pitch text temporary state.
            if isinstance(current_pitch, tuple):
                pitch_text = '%s%s' % current_pitch
                pitch_text_t0 = time.time()
            elif pitch_text_t0 and (time.time() - pitch_text_t0) > 2:
                pitch_text = ''
                pitch_text_t0 = None

            # Wash out prior screen.
            gameDisplay.fill(WHITE)

            # Lookup current image.
            if self.scroll:
                pass
            else:
                img = self.get_highlight_image(self.i)
                img_w, img_h = img.get_rect().size
                # print('Image size:', img_w, img_h)
                img_x, img_y = 0, 0
                if img_w == self.display_width and img_h < self.display_height:
                    # Center vertically.
                    img_y = int(round((self.display_height - img_h)/2))
                elif img_h == self.display_height and img_w < self.display_width:
                    # Center horizontally.
                    img_x = int(round((self.display_width - img_w)/2))
                gameDisplay.blit(img, (img_x, img_y))

            # Show play/paused status light.
            light_color = GREEN if self.playing else RED
            pygame.draw.circle(gameDisplay, light_color, (int(CIRCLE_DIAMETER*1.), int(CIRCLE_DIAMETER*1.)), int(CIRCLE_DIAMETER/2.))

            # Show the score at the bottom center.
            if self.td:
                seconds_ratio = int(round(self.total_seconds_expected/self.td*100))
                self.record_performance_log(path=self.path, score=seconds_ratio)
                text = 'Seconds: %s/%s = %i%%' % (int(round(self.total_seconds_expected)), int(round(self.td)), seconds_ratio)
                largeText = pygame.font.Font('freesansbold.ttf', 50)
                TextSurf, TextRect = text_objects(text, largeText)
                TextRect.center = ((self._display_width/2), self._display_height-BUFFER_H - 50)
                gameDisplay.blit(TextSurf, TextRect)

            # Show the current pitch in the lower lefthand corner.
            if pitch_text:
                largeText = pygame.font.Font('freesansbold.ttf', 50)
                TextSurf, TextRect = text_objects(pitch_text, largeText)
                TextRect.center = (50, self._display_height-BUFFER_H - 50)
                gameDisplay.blit(TextSurf, TextRect)

            # Show playlist position in bottom right corner.
            smallText = pygame.font.Font('freesansbold.ttf', 25)
            text = '%i/%i' % (self.playlist_position+1, len(self.playlist))
            TextSurf, TextRect = text_objects(text, smallText)
            text_width, text_height = smallText.size(text)
            TextRect.center = (self._display_width - text_width, self._display_height-BUFFER_H - text_height)
            gameDisplay.blit(TextSurf, TextRect)

            # Show control help text.
            if showing_help:
                smallText = pygame.font.Font('freesansbold.ttf', 25)
                text = 'q=quit p=play/pause r=reset <-/->=step n=next b=back'
                TextSurf, TextRect = text_objects(text, smallText)
                text_width, text_height = smallText.size(text)
                TextRect.center = (self._display_width/2, self._display_height-BUFFER_H - text_height)
                gameDisplay.blit(TextSurf, TextRect)

            # Show loading status message to denote potentially long data loads.
            if is_loading:
                s = pygame.Surface((self._display_width, self._display_height), pygame.SRCALPHA)
                s.fill((255, 255, 255, 128))
                gameDisplay.blit(s, (0, 0))
                largeText = pygame.font.Font('freesansbold.ttf', 50)
                text = 'Loading...'
                TextSurf, TextRect = text_objects(text, largeText)
                text_width, text_height = largeText.size(text)
                TextRect.center = (self._display_width/2, self._display_height/2 - text_height/2)
                gameDisplay.blit(TextSurf, TextRect)

            pygame.display.update()
            clock.tick(60)

            # Apply changes that we didn't want to play before we updated the screen, for fear that they'd delay screen updates.
            if playlist_change:
                playlist_change()
                playlist_change = None
                is_loading = False

        pygame.quit()
        sys.exit()


def get_scores():
    score_agg = defaultdict(dict) # {(year,week): {{path: [scores]}}
    sum_aggs = defaultdict(dict) # {(year,week): {{path: mean_score}}
    counts = defaultdict(dict) # {(year,week): {path: counts}}
    with open(LOG_FN, 'r') as fin:
        for line in fin.readlines():
            line = line.strip()
            if not line:
                continue
            timestamp, path, score = line.split(',')
            dt = parse(timestamp).date()
            week = dt.isocalendar()[1]
            if not path.startswith('/'):
                path = os.path.abspath(path)
            for key in [GLOBAL_DATE, (dt.year, week)]:
                score_agg[key].setdefault(path, [])
                score_agg[key][path].append(int(score))
                counts[key][path] = len(score_agg[key][path])
                sum_aggs[key][path] = int(round(sum(score_agg[key][path])/float(len(score_agg[key][path]))))
    return sum_aggs, counts


def show_scores():
    sum_aggs, counts = get_scores()
    for key in sorted(sum_aggs):
        year, week = key
        print()
        print('%s/%s:' % (year, week))
        print()
        print('Score\tError\tCount\tPath')
        for path, mean_score in sorted(sum_aggs[key].items(), key=lambda o: o[1]):
            count = counts[key][path]
            error = abs(100 - mean_score)
            print(f'{mean_score}\t{error}\t{count}\t{path}')


def main(paths, render_only=False, scores_only=False, reorder=True, scroll=False, web=False):

    if scores_only:
        show_scores()
        return

    if web:
        app.run()
        # webbrowser.open('http://0.0.0.0:5000')
        return

    # If we have past scoring, then sort by most error prone first so we practice those first.
    ordering = {}
    if reorder:
        try:
            score_aggs, path_counts = get_scores()
            last_key = sorted(k for k in score_aggs if k != GLOBAL_DATE)[-1]
            for path, mean_score in score_aggs[last_key].items():
                ordering[path] = abs(100 - mean_score)
            for path, mean_score in score_aggs[GLOBAL_DATE].items():
                if path not in ordering:
                    ordering[path] = abs(100 - mean_score)
            print('Ordering:')
            for path, order in sorted(ordering.items(), key=lambda o: o[1]):
                print(order, path)
        except IndexError:
            pass

    path = None
    if paths:
        path = paths[0]
    assert path, 'No .abc or .pls path specified!'
    game = Game(path, ordering=ordering, scroll=scroll)

    if render_only:
        for fn in game.playlist:
            logger.info('Rendering %s.', fn)
            notor = Notoriety(fn)
            notor.render_note_highlights()
        return

    game.run()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Tutors a music score.')
    parser.add_argument('path', nargs='*', help='Path to ABC music file.')
    parser.add_argument('--render-only', action='store_true', default=False, help='Render images then exit.')
    parser.add_argument('--scores-only', action='store_true', default=False, help='Show aggregate average scores then exit.')
    parser.add_argument('--scroll', action='store_true', default=False, help='Real-time side scrolling mode.')
    parser.add_argument('-o', action='store_true', dest='reorder', default=False, help='Auto-reorder list according to scores.')
    parser.add_argument('--web', action='store_true', default=False, help='Run web-version.')
    args = parser.parse_args()
    main(args.path, render_only=args.render_only, scores_only=args.scores_only, reorder=args.reorder, scroll=args.scroll, web=args.web)
