Notoriety
=========

![Build Status](https://gitlab.com/chrisspen/notoriety/badges/master/pipeline.svg)

Tutoring program for learning to play notes on a musical instrument.

Designed specifically for the violin, but should work with most instruments.

Usage
-----

Each tool revolves around a piece of sheet music, assumed to be in [ABC Notation](http://abcnotation.com/) format.

Web version:

    ./init_virtualenv.sh
    ./runserver

Desktop version:

Activate environment:

    ./init_virtualenv.sh
    . ./setup.bash

Play music through MIDI synthesizer (for debugging purposes):

    ./play_abc.py <filename.abc>

Render sheet music in PNG format:

    ./render_abc.py <filename.abc>

Listen to microphone and detect and display musical note:

    ./detect_note.py

Display a user interface that displays sheet music and tracks progress:

    ./tutor_abc.py <filename.abc>

Development
-----------

When modifying the Javascript, to automatically rebuild the NPM package for testing in a browser, run:

    ./build-js.sh
