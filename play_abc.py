#!/usr/bin/env python
import argparse

from notoriety import Notoriety

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Plays sheet music.')
    parser.add_argument('path', help='Path to ABC music file.')
    args = parser.parse_args()
    Notoriety(path=args.path).play()
