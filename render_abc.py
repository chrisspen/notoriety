#!/usr/bin/env python
import argparse

from notoriety import Notoriety


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Renders sheet music.')
    parser.add_argument('path', nargs='+', help='Path to ABC music file.')
    parser.add_argument('--show', action='store_true', default=False, help='Show in window, do not create file.')
    parser.add_argument('--svg', action='store_true', default=False, help='Render in SVG format.')
    args = parser.parse_args()
    for path in args.path:
        Notoriety(path=path).render(force=True, svg=args.svg, show=args.show)
