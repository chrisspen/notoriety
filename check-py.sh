#!/bin/bash
echo "[$(date)] Checking Python."
pylint --version
FILES=`find . \( ! -regex '.*/\..*' \) -type f -name '*.py' | grep -v pyparsing | grep -v abc2xml | xargs`
pylint --rcfile=pylint.rc $FILES
