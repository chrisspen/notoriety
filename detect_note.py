#!/usr/bin/env python
import argparse

from notoriety import Notoriety

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Displays current pitch detected on the microphone.')
    parser.add_argument('--show-volume', action='store_true', default=False, help='Printout the current volume level.')
    parser.add_argument('--volume-thresh', default=0.05, help='Printout the current volume level.')
    parser.add_argument('--min-frames', default=5, help='Minimum number of frames to see a note before accepting it.')
    args = parser.parse_args()

    Notoriety().listen(show=True, show_volume=args.show_volume, volume_thresh=float(args.volume_thresh), min_frames=int(args.min_frames))
